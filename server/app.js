const Koa = require('koa');
const app = new Koa();
 
// response 
app.use(ctx => {
    console.log(ctx.headers.connection);
    ctx.body = 'Hello KoaJS';
});

console.log("[server]: listen on 3000");
app.listen(3000);